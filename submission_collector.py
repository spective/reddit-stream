import json

import socket
import time

import prawcore.exceptions
from _pulsar import ConsumerType

from clients import pulsar_client, reddit_client

p_client = pulsar_client()
post_id_subscription = p_client.subscribe("persistent://spective/default/reddit-id", "submission-collector",
                                          consumer_type=ConsumerType.Shared)  # shared
reddit_post_producer = p_client.create_producer("persistent://spective/default/reddit-post", batching_enabled=True,
                                                batching_max_publish_delay_ms=100, block_if_queue_full=True)
reddit = reddit_client("submission_collector")
hostname = socket.gethostname()


def get_messages():
    messages = []
    for i in range(100):
        try:
            messages.append(post_id_subscription.receive(1000))
        except:
            break
    return messages


def acknowledge_messages(messages):
    for message in messages:
        post_id_subscription.acknowledge(message)


def format_reddit_submission(reddit_submission):
    submission = {
        'id': reddit_submission.id,
        'author': reddit_submission.author.name,
        'created_utc': reddit_submission.created_utc,
        'subreddit': reddit_submission.subreddit.display_name,
        'title': reddit_submission.title,
        'url': reddit_submission.url,
        'over_18': reddit_submission.over_18,
        'scrape_host': hostname,
        'scrape_time': int(time.time()),
        'source': 'reddit'
    }
    if 'is_gallery' in reddit_submission.__dict__:
        submission['is_gallery'] = reddit_submission.is_gallery
        submission['gallery_data'] = reddit_submission.gallery_data
        submission['media_metadata'] = reddit_submission.media_metadata
    return submission


import faulthandler


def process_messages(messages):
    ids = [x.data().decode("UTF-8") for x in messages]
    fullnames = ["t3_" + x for x in ids]
    processed_counter = 0
    for submission in reddit.info(fullnames):
        if submission.author is not None:
            processed_counter += 1
            formatted_submission = format_reddit_submission(submission)
            json_bytes = json.dumps(formatted_submission).encode("UTF-8")
            faulthandler.enable()
            reddit_post_producer.send(json_bytes)
    print(f"Processed {processed_counter} of {len(messages)} submissions")


if __name__ == '__main__':
    while True:
        try:
            m = get_messages()
            process_messages(m)
            acknowledge_messages(m)
        except prawcore.exceptions.ServerError:
            print("Server error")


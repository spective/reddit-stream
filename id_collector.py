import datetime
import time

import numpy as np
from concurrent.futures.thread import ThreadPoolExecutor

import clients
from clients import pulsar_client, reddit_client

pulsar_producer = pulsar_client().create_producer('persistent://spective/default/reddit-id', batching_enabled=True)

astra_client = clients.astra_client()
reddit = reddit_client("id_collector")
last_loaded = astra_client._rest_client.request(
    method="GET",
    path="/api/rest/v2/keyspaces/stream/checkpoint/id_collector",
    url_params={'page-size': 1, 'sort': '{"checkpoint_time":"desc"}'})
post_cursor = int(last_loaded['data'][0]['post_number'])


def save_checkpoint(post_number, post_id):
    astra_client.rest.add_row('stream', 'checkpoint',
                              {'name': 'id_collector', 'post_number': post_number,
                               'post_id': post_id,
                               'checkpoint_time': datetime.datetime.utcnow().replace(
                                   tzinfo=datetime.timezone.utc,
                                   microsecond=0).isoformat().replace("+00:00", "Z")})


if __name__ == '__main__':
    pool = ThreadPoolExecutor(max_workers=10)
    while True:
        time.sleep(1)
        for submission in reddit.subreddit('all').stream.submissions():
            most_recent_submission = submission
            break
        most_recent_id_number = int(most_recent_submission.id, 36)
        print(most_recent_id_number)
        print(most_recent_id_number - post_cursor)
        if most_recent_id_number > post_cursor:
            if most_recent_id_number-post_cursor > 100_000:
                most_recent_id_number = post_cursor+100_000
            for index in range(post_cursor, most_recent_id_number):
                pool.submit(pulsar_producer.send, np.base_repr(index, 36).lower().encode("UTF-8"))
                if (index - post_cursor) % 10_000 == 0:
                    pulsar_producer.flush()
                    print(f"Collected {index - post_cursor} ids")
                    save_checkpoint(index, np.base_repr(index, 36).lower())
            pulsar_producer.flush()
            save_checkpoint(most_recent_id_number, most_recent_submission.id)
            post_cursor = most_recent_id_number

import pulsar
import praw
from astrapy.client import create_astra_client
import os
import socket

hostname = socket.gethostname()


def pulsar_client():
    service_url = os.environ['PULSAR_URL']
    token = os.environ['PULSAR_TOKEN']
    return pulsar.Client(service_url, authentication=pulsar.AuthenticationToken(token))


def astra_client():
    astra_db_id = os.environ['ASTRA_DATABASEID']
    astra_db_region = os.environ['ASTRA_CLOUDREGION']
    astra_db_application_token = os.environ['ASTRA_APPLICATIONTOKEN']
    return create_astra_client(astra_database_id=astra_db_id,
                               astra_database_region=astra_db_region,
                               astra_application_token=astra_db_application_token)


def reddit_client(client_type):
    return praw.Reddit(
        client_id=os.environ['REDDIT_CLIENT_ID'],
        client_secret=os.environ['REDDIT_CLIENT_SECRET'],
        user_agent=f"reddit-stream:{client_type}:{hostname}"
    )
